﻿using System;
using System.Data.Entity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LearningProjectDatabase.Entities
{
    public class ContactFormData
    {
        public int Id { get; set; }
        [Required]
      
        public string Firstname { get; set; }
        [Required(ErrorMessage = "Please enter your firstname")]
        public string Lastname { get; set; }
        [Required(ErrorMessage = "Please enter your lastname")]
        public string Theme { get; set; }

        public string Personal { get; set; }

        public string Company { get; set; }

        public string Email { get; set; }
        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "You must enter a valid email address")]

        public string Message { get; set; }
        [Required(ErrorMessage = "Please enter your message")]
        [MaxLength(length:500, ErrorMessage = "Your message must be no loner than 500 characters")]

        public string AcceptDataAgreement { get; set; }
    }
}

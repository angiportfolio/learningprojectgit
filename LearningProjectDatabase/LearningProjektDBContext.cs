﻿using System.Data.Entity;
using System.Configuration;
using System.Data.Entity.ModelConfiguration.Conventions;
using LearningProjectDatabase.Entities;

namespace LearningProjectDatabase
{
    public class LearningProjektDBContext : DbContext
    {
        static LearningProjektDBContext()
        {
            Database.SetInitializer<LearningProjektDBContext>(null);
        }

        public DbSet<ContactFormData> ContactFormDataItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Arg: The convention says: CascadeDelete is on if your relationship/navigation property is "required" (=foreign key/reference are forbidden to be null). 
            // But it's off if your relationship/navigation is "optional" (=foreign key/reference are allowed to be null)
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }
    }
}

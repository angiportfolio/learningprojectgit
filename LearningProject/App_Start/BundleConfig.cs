﻿using System.Web.Mvc;
using System.Web.Optimization;

namespace LearningProject
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/main").Include(
                "~/Scripts/jquery-{version}.js",
                "~/Scripts/modernizr-*",
                "~/Scripts/bootstrap.js",
                "~/Scripts/main.js"));

            bundles.Add(new StyleBundle("~/bundles/main.css").Include(
                      "~/css/main.css"));
        }       
    }
}

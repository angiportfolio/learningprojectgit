﻿using System;
using System.Collections.Specialized;
using System.Net.Mail;
using System.Web.Mvc;
using LearningProjectDatabase;
using LearningProjectDatabase.Entities;
using Umbraco.Core.Logging;
using Umbraco.Web.Models;
using Umbraco.Web;
using Umbraco.Web.Mvc;
using Archetype.Models;
using System.Collections.Generic;

namespace LearningProject.Controllers
{
    public class ContactFormController : SurfaceController
    {

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitForm(RenderModel model)
        {
            ViewBag.FormSent = -1;
            try
            {
                ArchetypeModel recipientMail = model.Content.GetPropertyValue<ArchetypeModel>("themeRecipientEmail");
                List<string> emails = new List<string>();

                foreach (var mail in recipientMail.Fieldsets)
                {
                    emails.Add(mail.GetValue<string>("recipientEmail"));
                }

                string theme = "Contact Request"; // themeRecipientMail.Split('|')[1];
                var form = new NameValueCollection(Request.Form) { ["theme"] = theme };
                SaveContactFormData(form);
                ViewBag.FormSent = SendEmail(model, form, emails.ToString());
                 
            }
            catch (Exception ex)
            {
                LogHelper.Error<ContactFormController>("Error: ", ex);
            }

            return View(model);
        }


        private int SendEmail(RenderModel model, NameValueCollection form, string recipientMail)
        {
            int ret = -1;
            MailMessage message = new MailMessage(model.Content.GetPropertyValue<string>("Email"), "kangiex@gmail.com");
            message.Subject = string.Format("Enquiry from {0} {1} - {2}", model.Content.GetPropertyValue<string>("Firstname"), model.Content.GetPropertyValue<string>("Lastname"), model.Content.GetPropertyValue<string>("Email"));
            message.Body = model.Content.GetPropertyValue<string>("Message");

            var toAddr = recipientMail;
            string fromAddr = !String.IsNullOrEmpty(model.Content.GetPropertyValue<string>("from")) ? model.Content.GetPropertyValue<string>("from") : "no-reply@gmail.com";
            //var message = ParseMailMessage(fromAddr, toAddr, form);

            try
            {
                SmtpClient client = new SmtpClient();
                client.Send(message);
                LogHelper.Info<ContactFormController>(() => string.Format("Success: {0} {1} {2}", toAddr, message.From.Address, message.Body));
                ret = 1;
            }
            catch (Exception ex)
            {
                LogHelper.Error<ContactFormController>(string.Format("Error: {0} {1} {2}", toAddr, message.From.Address, message.Body), ex);
            }
            return ret;
        }


        private void SaveContactFormData(NameValueCollection form)
        {
            using (LearningProjektDBContext dbcontext = new LearningProjektDBContext())
            {
                ContactFormData contactFormDataItem = new ContactFormData
                {
                    Firstname = form["firstname"],
                    Lastname = form["lastname"],
                    Theme = form["theme"],
                    Personal = form["theme"],
                    Company = form["theme"],
                    Email = form["email"],
                    Message = form["message"],
                    AcceptDataAgreement = form["accept"]
                };
                dbcontext.ContactFormDataItems.Add(contactFormDataItem);
                dbcontext.SaveChanges();
            }
        }


        private bool DataPolicyAccepted(bool privacyPolicyEnabled)
        {
            if (privacyPolicyEnabled)
            {
                return !(string.IsNullOrEmpty(Request.Form["data-agreement"]));
            }
            else
            {
                return true;
            }
        }
    }
}